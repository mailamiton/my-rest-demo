package com.amit.utility;

/**
 * @author Crunchify.com
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/read")
public class readFileService {
	@GET
	@Produces("application/json")
	public Response readFile() {

		BufferedReader br = null;
		String sCurrentLine = null;
		String outputLine = null;
		try {

			br = new BufferedReader(new FileReader(
					"/home/user/Documents/NetmgicSSPTrobleshooting.txt"));

			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
				outputLine = sCurrentLine;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return Response.status(200).entity(outputLine).build();

	}
}