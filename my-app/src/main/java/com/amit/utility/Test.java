package com.amit.utility;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;
@Path("/test")
public class Test {

	  @GET
	  @Produces("application/json")
	  public Response convertFtoC() throws JSONException {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("amit", "1"); 
		jsonObject.put("kumar", "2");

		String result = jsonObject.toString();
		return Response.status(200).entity(result).build();
	  }
	  
	  @POST
	  @Produces("application/json")
	  @Consumes(MediaType.APPLICATION_JSON)
	  public Response convertPost(String accept) throws JSONException {
		  System.out.println("Input Parms " +  accept);

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("Post", "1"); 
		jsonObject.put("OUT", "2");

		String result = jsonObject.toString();
		return Response.status(200).entity(result).build();
	  }
}